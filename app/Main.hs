module Main where

import MC
import System.Random.MWC (createSystemRandom)

p :: Integer -> Integer -> Bool
p x y = x' ^ 2 + y' ^ 2 < 1
  where
    x' = (fromIntegral x - 0.25) / 3 :: Double
    y' = (fromIntegral y - 0.25) / 4

main :: IO ()
main =
  createSystemRandom >>= estStopTime 1000000 p 100000 >>= \res -> do
    case res of
      Nothing -> putStrLn "At least one sample exceeded maxstep."
      Just n -> putStrLn $ "E(T) = " ++ show n
