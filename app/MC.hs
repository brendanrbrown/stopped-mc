{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE DeriveGeneric #-}

module MC (estStopTime) where

import Control.Monad (replicateM)
import GHC.Generics (Generic)
import System.Random.Stateful

{- TYPES -}

-- | Type holding the state for a random walk in progress,
-- or the time at which it was stopped.
data RWState = Stop !Int | Continue !Integer !Integer
  deriving (Show)

-- | One-step for random walk on Z^2.
data RWStep = North | South | East | West
  deriving (Show, Eq, Generic)

instance Finite RWStep

instance Uniform RWStep

{- RW Transitions -}

-- | Check the condition and if False, set the stopping time.
setRW ::
  (Integer -> Integer -> Bool) ->
  Int ->
  Integer ->
  Integer ->
  RWState
setRW p n !x !y
  | p x y = Continue x y
  | otherwise = Stop n

stepRW ::
  (Integer -> Integer -> Bool) ->
  Int ->
  Integer ->
  Integer ->
  RWStep ->
  RWState
stepRW p n !x !y North = setRW p n x (y + 1)
stepRW p n !x !y South = setRW p n x (y - 1)
stepRW p n !x !y East = setRW p n (x + 1) y
stepRW p n !x !y West = setRW p n (x - 1) y

-- | Run a random walk on Z^2 for at most 'maxtep', so long as condition 'p' holds. Return
-- the first time at which 'p' is 'False' if that was reached before 'maxstep'.
walk ::
  StatefulGen g m =>
  Int ->
  (Integer -> Integer -> Bool) ->
  Int ->
  g ->
  RWState ->
  m RWState
walk _ _ _ _ (Stop n) = pure $ Stop n
walk maxstep p n gen (Continue x y) = do
  if n >= maxstep
    then pure (Continue x y)
    else do
      let n' = n + 1
      uniformM gen >>= walk maxstep p n' gen . stepRW p n' x y

-- | Sample N stopped random walks times, all with the same conditions.
walkN ::
  (StatefulGen g m) =>
  Int ->
  (Integer -> Integer -> Bool) ->
  RWState ->
  Int ->
  g ->
  m [RWState]
walkN n p init maxstep gen = replicateM n $ walk maxstep p 0 gen init

-- | Sample the random walk n times with initial condition (0, 0) and
-- estimate the expected stopping time only if all walks were stopped.
-- If this returns 'Nothing', try the sampler again with a larger 'maxstep'
-- if you are sure the expected stop time is finite.
estStopTime ::
  (StatefulGen g m) =>
  Int ->
  (Integer -> Integer -> Bool) ->
  Int ->
  g ->
  m (Maybe Double)
estStopTime n p maxstep gen = mean n <$> walkN n p (setRW p 0 0 0) maxstep gen
  where
    mean n' xs = (/ fromIntegral n') . fromIntegral . sum <$> traverse op xs
    op (Stop n) = Just n
    op _ = Nothing
